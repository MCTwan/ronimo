﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Wallet : MonoBehaviour
{
    [SerializeField]
    Text moneyTxt;
    public int money;
    // Start is called before the first frame update
    void Start()
    {
        money = 10;
    }

    // Update is called once per frame
    void Update()
    {
        moneyTxt.text = money.ToString() + "$";
    }
}
