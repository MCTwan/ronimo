﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Base : MonoBehaviour
{
    public float health;
    public Faction baseFaction;
    [SerializeField]
    GameObject spawner;
    [SerializeField]
    Transform nextSpawnPos;
    [SerializeField]
    bool isLastBase;
    [SerializeField]
    GameEnd end;
    // Start is called before the first frame update
    void Start()
    {
        health = 1000;
        if (isLastBase == true)
        {
            health += 1000;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (health <= 0)
        {
            Destroy(gameObject);
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Unit" && collision.transform.GetComponent<UnitManager>().faction != baseFaction)
        {
            Destroy(collision.gameObject);
            health -= 100;
        }
    }

    private void OnDestroy()
    {
        spawner.transform.position = nextSpawnPos.position;
        if (isLastBase == true)
        {
            end.GameOver(Faction.cowboiz);
        }

        if (baseFaction == Faction.cowboiz)
        {
            end.GameOver(Faction.banditos);
        }
    }
}
