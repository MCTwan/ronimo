﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ablities : MonoBehaviour
{
    const float cooldownEnd = 20;

    [SerializeField]
    GameObject barrel, tower, ablityTarget, targetArrow;
    [SerializeField]
    Transform barrelPos;
    [SerializeField]
    Image barrelimage, towerImage;

    float barrelcooldown = 0;
    float towercooldown = 0;

    bool barrelActiveded = false;
    bool towerActiveded = false;
    bool useAbilty = false;
    bool usedbarrel = false;
    bool usedtower = false;
    private void Start()
    {
        targetArrow.SetActive(false);
    }

    private void Update()
    {
        //when clicked set the target position the the clicked place and launch the barrel
        if (useAbilty == true)
        {
            targetArrow.SetActive(true);
            Vector3 mousePos = new Vector3(Input.mousePosition.x, 0, 0 - Camera.main.transform.position.z);
            mousePos = Camera.main.ScreenToWorldPoint(mousePos);
            ablityTarget.transform.position = new Vector3(mousePos.x, ablityTarget.transform.position.y, ablityTarget.transform.position.z);

            if (Input.GetButtonDown("Fire1"))
            {
                if (barrelActiveded == true)
                {
                    GameObject thrownBarrel = Instantiate(barrel, barrelPos.position, Quaternion.identity);
                    thrownBarrel.GetComponent<ExplosionBarrel>().targetPosition = ablityTarget.transform;
                    barrelActiveded = false;
                    usedbarrel = true;
                }

                if (towerActiveded)
                {
                    GameObject spawnedTower = Instantiate(tower, new Vector3(ablityTarget.transform.position.x, 0, ablityTarget.transform.position.z), Quaternion.identity);
                    towerActiveded = false;
                    usedtower = true;
                }

                useAbilty = false;
                targetArrow.SetActive(false);
            }
        }

        //cooldown
        if (usedbarrel == true)
        {
            barrelcooldown += Time.deltaTime;
            barrelimage.fillAmount = barrelcooldown / cooldownEnd;
            if (barrelcooldown >= cooldownEnd)
            {
                barrelcooldown = 0;
                usedbarrel = false;
            }
        }

        if (usedtower == true)
        {
            towercooldown += Time.deltaTime;
            towerImage.fillAmount = towercooldown / cooldownEnd;
            if (towercooldown >= cooldownEnd)
            {
                towercooldown = 0;
                usedtower = false;
            }
        }
    }

    public void ExplosiveBarrel()
    {
        if (usedbarrel == false)
        {
            useAbilty = true;
            barrelActiveded = true;
        }
    }

    public void SpawnTower()
    {
        if (usedtower == false)
        {
            useAbilty = true;
            towerActiveded = true;
        }
    }
}
