﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    [SerializeField] private GameObject sniperGraphic;

    private bool ocupied = false;
    private void Update()
    {
        Collider[] friendlyUnitsInRange = Physics.OverlapSphere(transform.position, 5f);
        Collider[] enemyUnitsInRange = Physics.OverlapSphere(transform.position, 5f);
        if (ocupied)
        {
            GetComponent<Attack>().enabled = true;
            sniperGraphic.GetComponent<SpriteRenderer>().enabled = true;
        }
        else
        {
            sniperGraphic.GetComponent<SpriteRenderer>().enabled = false;
            GetComponent<Attack>().enabled = false;
            if (friendlyUnitsInRange.Length > 0)
            {
                for (int i = 0; i < friendlyUnitsInRange.Length; i++)
                {
                    if (friendlyUnitsInRange[i].gameObject.GetComponent<UnitManager>() != null)
                    {
                        if (friendlyUnitsInRange[i].gameObject.GetComponent<UnitManager>().unit == Unit.sniper && friendlyUnitsInRange[i].gameObject.GetComponent<UnitManager>().faction == Faction.cowboiz)
                        {
                            Destroy(friendlyUnitsInRange[i].gameObject);
                            ocupied = true;
                        }
                    }
                }
            }
        }
    }
}
