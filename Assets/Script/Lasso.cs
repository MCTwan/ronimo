﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lasso : MonoBehaviour
{
    [SerializeField]
    float range;
    [SerializeField]
    float attackPower;

    float attackTimer;
    [SerializeField]
    float attackEnd;

    Move move;
    UnitManager targedUnit;

    void Start()
    {
        move = GetComponent<Move>();
    }

    void Update()
    {
        RaycastHit2D[] hits = Physics2D.RaycastAll(transform.position, transform.right, range);

        for (int hit = 0; hit < hits.Length; hit++)
        {
            if (hits[hit].transform.tag == "Unit" && hits[hit].transform != transform)
            {
                targedUnit = hits[hit].transform.GetComponent<UnitManager>();
                move.move = false;
            }
            else
            {
                move.move = true;
            }
        }

        if (move.move == false)
        {
            attackTimer += Time.deltaTime;
        }
        else
        {
            attackTimer = 0;
        }

        if (attackTimer >= attackEnd && Vector3.Distance(targedUnit.transform.position, transform.position) > 2.2f)
        {
            targedUnit.transform.position = Vector3.MoveTowards(targedUnit.transform.position, new Vector3(transform.position.x + 2.2f, transform.position.y, transform.position.z), Time.deltaTime * 10);
        }
    }
}
