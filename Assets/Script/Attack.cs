﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour
{
    [SerializeField]
    float range;
    [SerializeField]
    float attackPower;

    float attackTimer;
    [SerializeField]
    float attackEnd;

    Move move;
    UnitManager unitManager;
    UnitManager targedUnit;
    Base targetBase;
    Animator animator;
    float aimDirection;
    // Start is called before the first frame update
    void Start()
    {
        move = GetComponent<Move>();
        unitManager = GetComponent<UnitManager>();
        animator = GetComponent<Animator>();
        targedUnit = null;
        switch (unitManager.faction)
        {
            case Faction.cowboiz:
                aimDirection = 1;
                break;
            case Faction.banditos:
                aimDirection = -1;
                break;
            default:
                break;
        }
    }

    void Update()
    {
        RaycastHit[] hits = Physics.RaycastAll(transform.position, transform.right * aimDirection, range);

        //if another unit is hit and it not the same faction the start attacking
        for (int hit = 0; hit < hits.Length; hit++)
        {
            if (hits[hit].transform.tag == "Unit" && hits[hit].transform != transform && hits[hit].transform.GetComponent<UnitManager>().faction != unitManager.faction)
            {
                targedUnit = hits[hit].transform.GetComponent<UnitManager>();
                move.move = false;
            }
            else
            {
                move.move = true;
            }
        }

        //if you have no target start moveing again
        if (targedUnit == null)
        {
            move.move = true;
        }

        //if you not moveing you're attacking so set the count the cooldown timer
        if (move.move == false)
        {
            attackTimer += Time.deltaTime;
        }

        //attack
        if (attackTimer >= attackEnd)
        {
            if (animator == null)
            {
                DealDamage();
            }
            else
            {
                animator.SetTrigger("Shoot");
            }
            attackTimer = 0;
        }
    }

    public void DealDamage()
    {
        if (targetBase != null)
        {
            targetBase.health -= attackPower;
        }
        else
        {
            targedUnit.health -= attackPower;
        }
    }
}
