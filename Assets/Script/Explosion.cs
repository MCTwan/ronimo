﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    const float power = 40;
    public Faction ownerFaction;
    [SerializeField]
    SpriteRenderer spriteRenderer;
    [SerializeField]
    AudioClip explosion;
    private void Start()
    {
        GameObject.FindObjectOfType<AudioSource>().PlayOneShot(explosion);
    }

    void Update()
    {
        //fade
        spriteRenderer.color -= new Color(0, 0, 0, Time.deltaTime);
        if (spriteRenderer.color.a <= .01f)
        {
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        //deal damage to enemy but not to self
        if (other.tag == "Unit" && ownerFaction != other.GetComponent<UnitManager>().faction)
        {
            other.GetComponent<UnitManager>().health -= power;
        }
    }
}
