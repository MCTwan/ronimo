﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour
{
    [SerializeField]
    GameObject miner, gunSlinger, strongman, sniper, drunk, dynamite;
    [SerializeField]
    Transform spawnPos, minePos;
    [SerializeField]
    Wallet wallet;
    [SerializeField]
    AIOpponent opponent;

    int minercost = 10;
    int drunkcost = 12;
    int snipercost = 18;
    int gunslingercost = 15;
    int strongmancost = 30;
    int dynamitecost = 23;

    int minercount;

    public void SpawnMiner()
    {
        if (wallet.money >= minercost && minercount < 15)
        {
            GameObject spawnedMiner = Instantiate(miner, spawnPos.position, Quaternion.identity);
            spawnedMiner.GetComponent<Miner>().mine = minePos;
            spawnedMiner.GetComponent<Miner>().wallet = wallet;
            spawnedMiner.GetComponent<Miner>().sallon = spawnPos;
            wallet.money -= minercost;
            opponent.playerMiners++;
            minercount++;
        }
    }

    public void SpawnDrunk()
    {
        if (wallet.money >= drunkcost)
        {
            Instantiate(drunk, spawnPos.position, Quaternion.identity);
            wallet.money -= drunkcost;
        }
    }

    public void SpawnSniper()
    {
        if (wallet.money >= snipercost)
        {
            Instantiate(sniper, spawnPos.position, Quaternion.identity);
            wallet.money -= snipercost;
        }
    }

    public void SpawnGunSlinger()
    {
        if (wallet.money >= gunslingercost)
        {
            Instantiate(gunSlinger, spawnPos.position, Quaternion.identity);
            wallet.money -= gunslingercost;
        }
    }

    public void SpawnStrongMan()
    {
        if (wallet.money >= strongmancost)
        {
            Instantiate(strongman, spawnPos.position, Quaternion.identity);
            wallet.money -= strongmancost;
        }
    }

    public void SpawnDynamite()
    {
        if (wallet.money >= dynamitecost)
        {
            Instantiate(dynamite, spawnPos.position, Quaternion.identity);
            wallet.money -= dynamitecost;
        }
    }
}
