﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

enum Action
{
    start,
    options,
    quit
}

public class Buttons : MonoBehaviour
{
    const float rotationSpeed = 600;
    bool rotate;
    [SerializeField]
    GameObject play, options, quit;
    AudioSource audioSauce;
    [SerializeField]
    AudioClip buttonactionsound;
    Transform rotationObject;
    Action action;
    // Start is called before the first frame update
    void Start()
    {
        rotate = false;
        GameObject holder = Instantiate(new GameObject(), transform.position, Quaternion.identity);
        audioSauce = FindObjectOfType<AudioSource>();
        holder.AddComponent<OptionValueHolder>();
        holder.name = "holder";
    }

    // Update is called once per frame
    void Update()
    {
        if (rotate == true)
        {
            Rotation(ref rotationObject);
        }
    }

    public void StartGame()
    {
        rotationObject = play.transform;
        action = Action.start;
        rotate = true;
        audioSauce.PlayOneShot(buttonactionsound);
    }

    public void Options()
    {
        rotationObject = options.transform;
        action = Action.options;
        rotate = true;
        audioSauce.PlayOneShot(buttonactionsound);
    }

    public void Quit()
    {
        rotationObject = quit.transform;
        action = Action.quit;
        rotate = true;
        audioSauce.PlayOneShot(buttonactionsound);
    }

    public void Back()
    {
        SceneManager.LoadScene(0);
    }

    void Rotation(ref Transform rotationTarget)
    {
        rotationTarget.rotation = Quaternion.RotateTowards(rotationTarget.rotation, Quaternion.Euler(90, 0, 0), Time.deltaTime * rotationSpeed);
        if (rotationTarget.rotation == Quaternion.Euler(90, 0, 0))
        {
            switch (action)
            {
                case Action.start:           
                    SceneManager.LoadScene(1);
                    break;
                case Action.options:
                    SceneManager.LoadScene(2);
                    break;
                case Action.quit:
                    Application.Quit();
                    break;
                default:
                    break;
            }
        }
    }
}
