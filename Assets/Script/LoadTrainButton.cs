﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadTrainButton : MonoBehaviour
{
    [SerializeField]
    StationCollider station;
    [SerializeField]
    Train train;
    [SerializeField]
    Sprite norm, hover, clicked;
    SpriteRenderer rend;

    private void Start()
    {
        rend = GetComponent<SpriteRenderer>();
    }

    private void OnMouseOver()
    {
        //set sprite to hover
        rend.sprite = hover;
        //load the units in the train
        if (Input.GetButtonDown("Fire1") && station.unitsNearStation[0].GetComponent<UnitManager>().faction == Faction.cowboiz)
        {
            train.AddToTrain(station.unitsNearStation[0]);
            station.unitsNearStation.Remove(station.unitsNearStation[0]);
            rend.sprite = clicked;
        }
    }

    private void OnMouseExit()
    {
        //set sprite to normal sprite
        rend.sprite = norm;
    }
}
