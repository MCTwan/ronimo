﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Unit
{
    gunSlinger,
    drunk,
    sniper,
    goldCollector,
    strongman,
    dynamite
}

public class UnitManager : MonoBehaviour
{
    [SerializeField]
    GameObject deathCloud;
    public float health;
    public Unit unit;
    public Faction faction = Faction.cowboiz;
    int direction;
    Move move;
    // Start is called before the first frame update
    void Start()
    {
        move = GetComponent<Move>();
        switch (faction)
        {
            case Faction.cowboiz:
                direction = 1;
                break;
            case Faction.banditos:
                direction = -1;
                transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
                break;
            default:
                break;
        }
        move.direction = direction;
    }

    // Update is called once per frame
    void Update()
    {
        if (health <= 0)
        {
            Instantiate(deathCloud, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }
}
