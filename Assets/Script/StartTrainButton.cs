﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartTrainButton : MonoBehaviour
{
    [SerializeField]
    LoadTrainButton trainButton;
    [SerializeField]
    GameObject train, trainend, panel;
    [SerializeField]
    TMPro.TMP_Dropdown stationSelect;
    [SerializeField]
    Sprite norm, hover, click;
    [SerializeField]
    SpriteRenderer renderer;
    Train trainScript;
    bool startTrain;
    float treinSpeed = 18;

    private void Start()
    {
        panel.SetActive(false);
        trainScript = train.GetComponent<Train>();
    }

    private void Update()
    {
        if (trainend == null)
        {
            return;
        }

        if (startTrain == true)
        {
            train.transform.position = Vector3.MoveTowards(train.transform.position, trainend.transform.position, Time.deltaTime * treinSpeed);
        }

        if (train.transform.position == trainend.transform.position)
        {
            startTrain = false;
            for (int i = 0; i < trainScript.unitsInTrain.Count; i++)
            {
                trainScript.unitsInTrain[i].transform.parent = null;
                trainScript.unitsInTrain[i].transform.position = new Vector3(trainScript.unitsInTrain[i].transform.position.x, trainScript.unitsInTrain[i].transform.position.y, trainScript.unitTransforms[i].zPos);
                trainScript.unitsInTrain[i].GetComponent<Move>().move = true;
                if (trainScript.unitsInTrain[i].name == "good_dynamite_Trower(Clone)")
                {
                    trainScript.unitsInTrain[i].GetComponent<Dynamite_trower>().enabled = true;
                }
                else
                {
                    trainScript.unitsInTrain[i].GetComponent<Attack>().enabled = true;
                }
                trainScript.unitsInTrain[i].transform.localScale = trainScript.unitTransforms[i].scale;
                trainend = null;
            }

            trainScript.ClearTrain();
            GetComponent<SpriteRenderer>().enabled = true;
        }
    }

    private void OnMouseOver()
    {
        renderer.sprite = hover;
        if (Input.GetButtonUp("Fire1"))
        {
            panel.SetActive(true);
            renderer.sprite = click;
        }
    }

    private void OnMouseExit()
    {
        renderer.sprite = norm;
    }

    public void StartTrain()
    {
        trainScript.station = (Station)stationSelect.value;
        trainend = GameObject.Find(stationSelect.options[stationSelect.value].text);
        panel.SetActive(false);
        GetComponent<SpriteRenderer>().enabled = false;
        startTrain = true;
    }
}
