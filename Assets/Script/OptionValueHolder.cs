﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OptionValueHolder : MonoBehaviour
{
    static OptionValueHolder holder;
    public float volume;
    public float camScrollSpeed;
    public AudioSource audioSource;

    private void Awake()
    {
        volume = 1;
        camScrollSpeed = 50;
        //singelton
        if (OptionValueHolder.holder == null)
        {
            OptionValueHolder.holder = this;
        }
        else
        {
            if (OptionValueHolder.holder != this)
            {
                Destroy(gameObject);
            }
        }
        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        audioSource = FindObjectOfType<AudioSource>();
    }

    public void NewAdioSource()
    {
        audioSource = null;
    }

    private void Update()
    {
        if (SceneManager.GetActiveScene().buildIndex == 1 && audioSource == null)
        {
            audioSource = FindObjectOfType<AudioSource>();
        }

        audioSource.volume = volume;
    }
}
