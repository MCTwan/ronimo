﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSetup : MonoBehaviour
{
    [SerializeField]
    Texture2D cursorimage;
    Vector2 cursorHotspot;
    // Start is called before the first frame update
    void Start()
    {
        cursorHotspot = new Vector2(cursorimage.width / 2, cursorimage.height / 2);
        Cursor.SetCursor(cursorimage, cursorHotspot, CursorMode.Auto);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
