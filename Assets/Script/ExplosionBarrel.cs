﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionBarrel : MonoBehaviour
{
    const float speed = 60;

    [SerializeField]
    private AudioClip explosionSound;

    [SerializeField]
    private Sprite explosion;

    public Transform targetPosition;

    private Vector3 launchPosition;
    private Vector3 middlePosition;
    private Vector3 preMiddlePosition;
    private Vector3 postMiddlePosition;
    private Vector3 currentMoveTargetPosition;

    private SpriteRenderer spriteRenderer;

    private bool sound = false;

    void Start()
    {
        launchPosition = transform.position;
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        Launch();
        transform.position = Vector3.MoveTowards(transform.position, currentMoveTargetPosition, Time.deltaTime * speed);

        if (transform.position.x < preMiddlePosition.x)
        {
            currentMoveTargetPosition = preMiddlePosition;
        }
        if (transform.position.x >= preMiddlePosition.x && transform.position.x < middlePosition.x)
        {
            currentMoveTargetPosition = middlePosition;
        }
        if (transform.position.x >= middlePosition.x && transform.position.x < targetPosition.position.x)
        {
            currentMoveTargetPosition = postMiddlePosition;
        }
        if (transform.position.x >= postMiddlePosition.x)
        {
            currentMoveTargetPosition = targetPosition.position;
        }
        if (transform.position.x >= targetPosition.position.x)
        {
            //transform.position = launchPosition.position;
            Explode();
        }
    }
    private void Launch()
    {
        middlePosition = (launchPosition + targetPosition.position) / 2;
        preMiddlePosition = (launchPosition + middlePosition) / 2;
        postMiddlePosition = (middlePosition + targetPosition.position) / 2;
        middlePosition += new Vector3(0, 9.5f, 0);
        postMiddlePosition += new Vector3(0, 7.5f, 0);
        preMiddlePosition += new Vector3(0, 7.5f, 0);

    }
    private void Explode()
    {
        spriteRenderer.sprite = explosion;
        gameObject.AddComponent<Fade>();
        if (sound == false)
        {
            GameObject.FindObjectOfType<AudioSource>().PlayOneShot(explosionSound);
            sound = true;
        }
        Collider[] objectsToDestroy = Physics.OverlapSphere(transform.position, 10);
        for (int i = 0; i < objectsToDestroy.Length; i++)
        {
            if (objectsToDestroy[i].tag == "Unit")
            {
                if (objectsToDestroy[i].GetComponent<UnitManager>().faction == Faction.banditos)
                {
                    Destroy(objectsToDestroy[i].gameObject);
                }
            }
        }
    }
}
