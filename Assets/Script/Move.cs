﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Faction
{
    cowboiz,
    banditos,
    none
}

public class Move : MonoBehaviour
{
    public bool move;
    [SerializeField]
    float movementSpeed = 1;
    Animator animator;
    public int direction;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (move == true)
        {
            transform.position += transform.right * direction * (Time.deltaTime * movementSpeed);
            animator.SetBool("Walk", true);
        }
        else
        {
            animator.SetBool("Walk", false);
        }
    }
}
