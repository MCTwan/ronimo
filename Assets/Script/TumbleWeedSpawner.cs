﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TumbleWeedSpawner : MonoBehaviour
{
    public float SpawnTime;
    public float timer;
    [SerializeField]
    GameObject tumbleweed;
    // Start is called before the first frame update
    void Start()
    {
        SpawnTime = Random.Range(0, 40);
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (timer >= SpawnTime)
        {
            Instantiate(tumbleweed, transform.position, Quaternion.identity);
            SpawnTime = Random.Range(0, 40);
            timer = 0;
        }
    }
}
