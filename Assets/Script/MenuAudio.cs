﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuAudio : MonoBehaviour
{
    static MenuAudio menuAudio;
    private void Awake()
    {
        //singelton
        if (MenuAudio.menuAudio == null)
        {
            MenuAudio.menuAudio = this;
        }
        else
        {
            if (MenuAudio.menuAudio != this)
            {
                Destroy(gameObject);
            }
        }
        DontDestroyOnLoad(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        if (SceneManager.GetActiveScene().buildIndex == 1)
        {
            GameObject.Find("holder").GetComponent<OptionValueHolder>().NewAdioSource();
            Destroy(gameObject);
        }
    }
}
