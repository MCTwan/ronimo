﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIOpponent : MonoBehaviour
{
    public int money = 0;
    float buyTimer = 5;
    float minerBuyTimer = 60;
    float currentMinerBuyTimer;

    int miners = 1;
    public int playerMiners;

    float minerSpeed = 6.2f;
    float minerCooldown;

    [SerializeField] GameObject goldColector, gunslinger, dagger, dynamite, sniper, largeBandito;
    private void Start()
    {
        minerCooldown = minerSpeed;
        currentMinerBuyTimer = minerBuyTimer;
    }
    void Update()
    {
        UnitBuying();
        currentMinerBuyTimer -= Time.deltaTime;
        minerCooldown -= Time.deltaTime;
        if (minerCooldown <= 0)
        {
            money += 5 * miners;
            minerCooldown = minerSpeed;
        }
    }
    void UnitBuying()
    {
        buyTimer -= Time.deltaTime;
        if (buyTimer <= 0)
        {
            if ((miners * 2 < playerMiners && money >= 10) || (money >= 10 && currentMinerBuyTimer <= 0))
            {
                //BuyUnit(goldColector, 10);
                miners += 1;
                money -= 10;
                currentMinerBuyTimer = minerBuyTimer;
            }
            else if (money >= 12 && money < 15 && miners <= 5)
            {
                BuyUnit(dagger, 12);
            }
            else if (money >= 15 && money < 18)
            {
                BuyUnit(gunslinger, 15);
            }
            else if (money >= 18 && money < 23)
            {
                BuyUnit(sniper, 18);
            }
            else if (money >= 23 && money < 30)
            {
                BuyUnit(dynamite, 23);
            }
            else if (money >= 30)
            {
                BuyUnit(largeBandito, 30);
            }
            buyTimer = 5;
        }
        RaycastHit hit;
        if (Physics.Raycast(transform.position, Vector3.left, out hit, 5))
        {
            if (hit.collider.gameObject.GetComponent<UnitManager>().faction == Faction.cowboiz)
            {
                if (money >= 12)
                {
                    BuyUnit(dagger, 12);
                }
            }
        }
    }
    void BuyUnit(GameObject unitToSpawn, int price)
    {
        GameObject uiq;
        uiq = Instantiate(unitToSpawn, transform.position, Quaternion.identity);
        uiq.GetComponent<UnitManager>().faction = Faction.banditos;
        money -= price;
    }
}
