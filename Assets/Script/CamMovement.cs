﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamMovement : MonoBehaviour
{
    [SerializeField]
    float edgeoffset;

    float scrollSpeed;
    Vector3 desirecPos;
    OptionValueHolder holder;
    private void Start()
    {
        desirecPos = transform.position;
        if (false)
        {
            holder = GameObject.Find("holder").GetComponent<OptionValueHolder>();
            scrollSpeed = holder.camScrollSpeed;
        }
        else
        {
            scrollSpeed = 50;
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.mousePosition.x >= Screen.width * edgeoffset)
        {
            desirecPos += Vector3.right * (Time.deltaTime * scrollSpeed);
            Vector3 newPos = new Vector3(Mathf.Clamp(desirecPos.x, -53, 358), 1, -14.43f);
            transform.position = newPos;
        }
        if (Input.mousePosition.x <= Screen.width * (1 - edgeoffset))
        {
            desirecPos += Vector3.left * (Time.deltaTime * scrollSpeed);
            Vector3 newPos = new Vector3(Mathf.Clamp(desirecPos.x, -53, 358), 1, -14.43f);
            transform.position = newPos;
        }
    }
}
