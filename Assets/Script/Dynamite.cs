﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dynamite : MonoBehaviour
{
    const float throwPower = 8;
    public Vector3 forward;
    public GameObject owner;
    [SerializeField]
    GameObject explosion;
    Faction faction;
    void Start()
    {
        GetComponent<Rigidbody>().AddForce(forward * throwPower + Vector3.up * throwPower, ForceMode.Impulse);
        faction = owner.GetComponent<UnitManager>().faction;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Unit" && collision.transform.GetComponent<UnitManager>().faction != owner.GetComponent<UnitManager>().faction)
        {
            GameObject dynamite_Explosion = Instantiate(explosion, transform.position, Quaternion.identity);
            dynamite_Explosion.GetComponent<Explosion>().ownerFaction = faction;
            Destroy(gameObject);
        }
        else if (collision.transform.tag != "Unit" && collision.transform.tag != "dynamite")
        {
            GameObject dynamite_Explosion = Instantiate(explosion, transform.position, Quaternion.identity);
            dynamite_Explosion.GetComponent<Explosion>().ownerFaction = Faction.none;
            Destroy(gameObject);
        }
    }
}
