﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dynamite_trower : MonoBehaviour
{
    [SerializeField]
    float range;
    [SerializeField]
    float attackEnd;

    [SerializeField]
    bool isBandito;

    [SerializeField]
    GameObject dynamite;
    [SerializeField]
    Transform spawnPos;

    float attackTimer;

    Move move;
    UnitManager unitManager;
    UnitManager targedUnit;

    Animator animator;

    float throwDirection;
    void Start()
    {
        move = GetComponent<Move>();
        unitManager = GetComponent<UnitManager>();
        animator = GetComponent<Animator>();
        if (isBandito == true)
        {
            throwDirection = -1;
        }
        else
        {
            throwDirection = 1;
        }
    }

    void Update()
    {
        //check for enemies
        RaycastHit[] hits = Physics.RaycastAll(transform.position, transform.right, range);

        for (int hit = 0; hit < hits.Length; hit++)
        {
            if (hits[hit].transform.tag == "Unit" && hits[hit].transform != transform && hits[hit].transform.GetComponent<UnitManager>().faction != unitManager.faction)
            {
                targedUnit = hits[hit].transform.GetComponent<UnitManager>();
                move.move = false;
            }
        }

        if (targedUnit == null)
        {
            move.move = true;
        }


        //attack timer 
        if (move.move == false)
        {
            attackTimer += Time.deltaTime;
        }
        else
        {
          //  attackTimer = 0;
        }

        //attack
        if (attackTimer >= attackEnd)
        {
            animator.SetTrigger("Shoot");
            attackTimer = 0;
        }
    }

    public void Throw()
    {
        GameObject thrownDynamite = Instantiate(dynamite, spawnPos.position, Quaternion.identity);
        thrownDynamite.GetComponent<Dynamite>().forward = transform.right * throwDirection;
        thrownDynamite.GetComponent<Dynamite>().owner = gameObject;
    }
}
