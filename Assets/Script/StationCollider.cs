﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StationCollider : MonoBehaviour
{
    public List<GameObject> unitsNearStation;
    [SerializeField]
    GameObject button;
    [SerializeField]
    Train train;
    [SerializeField]
    Station thisStation;
    [SerializeField]
    StationManager stationManager;
    [SerializeField]
    int stationNum;

    bool activeStation = false;
    private void Update()
    {
        if (unitsNearStation.Count != 0 && train.station == thisStation)
        {
            button.SetActive(true);
            for (int i = 0; i < unitsNearStation.Count; i++)
            {
                if (unitsNearStation[i] == null)
                {
                    unitsNearStation.Remove(unitsNearStation[i]);
                }
            }
        }
        else
        {
            button.SetActive(false);
            unitsNearStation.Clear();
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Unit")
        {
            unitsNearStation.Add(other.gameObject);
            if (activeStation == false)
            {
                stationManager.AddStation(stationNum);
                activeStation = true;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        unitsNearStation.Remove(other.gameObject);
    }
}
