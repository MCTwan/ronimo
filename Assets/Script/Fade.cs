﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fade : MonoBehaviour
{
    Renderer rend;
    void Start()
    {
        rend = GetComponent<Renderer>();
    }

    void Update()
    {
        //fade by lowering the a and resize the gameobejct a bit
        transform.localScale += new Vector3(Time.deltaTime * .4f, Time.deltaTime * .4f, Time.deltaTime * .4f);
        rend.material.color -= new Color(0, 0, 0, Time.deltaTime);
        //if the a becomes invisable destroy the gameobject
        if (rend.material.color.a <= .3)
        {
            Destroy(gameObject);
        }
    }
}
