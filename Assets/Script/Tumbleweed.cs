﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tumbleweed : MonoBehaviour
{
    float timer;
   const float alivetime = 85;
    Vector3 veloctiy;
    Rigidbody rigidbody;
    // Start is called before the first frame update
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.AddForce(transform.right * 10, ForceMode.Impulse);
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        //set velocty
        if (timer >= 10)
        {
            if (veloctiy == Vector3.zero)
            {
                veloctiy = rigidbody.velocity;
            }
            rigidbody.velocity = veloctiy;
        }
        //kill
        if (timer >= alivetime)
        {
            Destroy(gameObject);
        }
    }
}
