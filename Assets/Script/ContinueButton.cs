﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ContinueButton : MonoBehaviour
{
    [SerializeField]
    Sprite norm, hover, clicked;
    SpriteRenderer rend;

    private void Start()
    {
        rend = GetComponent<SpriteRenderer>();
    }

    private void OnMouseOver()
    {
        //set sprite to hover
        rend.sprite = hover;

        //set clicked
        if (Input.GetButtonDown("Fire1"))
        {
            rend.sprite = clicked;
            SceneManager.LoadScene(0);
        }
    }

    private void OnMouseExit()
    {
        //set sprite to normal sprite
        rend.sprite = norm;
    }
}
