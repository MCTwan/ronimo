﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StationManager : MonoBehaviour
{
    [SerializeField]
    Sprite meverickStation, churchOfFaith, banditoHideOut;
    [SerializeField]
    TMPro.TMP_Dropdown stationDropdown;

    public void AddStation(int stationNum)
    {
        switch (stationNum)
        {
            case 0:
                stationDropdown.options.Add(new TMPro.TMP_Dropdown.OptionData() { text = "NearFirstChurch", image = meverickStation });
                break;
            case 1:
                stationDropdown.options.Add(new TMPro.TMP_Dropdown.OptionData() { text = "AfterFirstChurch", image = churchOfFaith });
                break;
            case 2:
                stationDropdown.options.Add(new TMPro.TMP_Dropdown.OptionData() { text = "NearLastChurch", image = banditoHideOut });
                break;
            default:
                break;
        }
    }
}
