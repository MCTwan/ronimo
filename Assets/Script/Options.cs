﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Options : MonoBehaviour
{
    [SerializeField]
    Slider volume, scrollSpeed;

    OptionValueHolder holder;

    void Start()
    {
        holder = GameObject.Find("holder").GetComponent<OptionValueHolder>();
        volume.value = holder.volume;
        scrollSpeed.value = holder.camScrollSpeed/100;
    }

    // Update is called once per frame
    void Update()
    {
        //audio
        holder.volume = volume.value;
        //scroll speed
        holder.camScrollSpeed = scrollSpeed.value * 100;
    }
}
