﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Miner : MonoBehaviour
{
    public Transform mine, sallon;
    public Wallet wallet;
    Vector3 target;
    float mineTimer;
    float endMineTimer;
    int coin;
    Animator animator;
    bool move;
    // Start is called before the first frame update
    void Start()
    {
        target = new Vector3(mine.position.x, transform.position.y, transform.position.z);
        animator = GetComponent<Animator>();
        endMineTimer = 4;
        move = true;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, target, Time.deltaTime * 6);

        if (transform.position == new Vector3(mine.position.x, transform.position.y, transform.position.z))
        {
            move = false;
            mineTimer += Time.deltaTime;
            if (mineTimer >= endMineTimer)
            {
                animator.SetTrigger("Shoot");
                mineTimer = 0;             
            }
        }
        else if (transform.position == new Vector3(sallon.position.x, transform.position.y, transform.position.z))
        {
            wallet.money += coin;
            target = new Vector3(mine.position.x, transform.position.y, transform.position.z);
            transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
        }

        if (move == true)
        {
            animator.SetBool("Walk", true);
        }
        else
        {
            animator.SetBool("Walk", false);
        }
    }

    public void Collect()
    {
        coin = 5;
        target = new Vector3(sallon.position.x, transform.position.y, transform.position.z);
        transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
        move = true;
    }
}
