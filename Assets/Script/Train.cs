﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Station
{
    saloon,
    maverickStation,
    churchOfFaith,
    banditoHideout
}

public class Train : MonoBehaviour
{
    public Station station;
    public List<GameObject> unitsInTrain;
    public List<UnitTransform> unitTransforms;
    [SerializeField]
    Transform[] trainPoint;
    int count;

    void Start()
    {
        station = Station.saloon;
        unitsInTrain = new List<GameObject>();
        unitTransforms = new List<UnitTransform>();
    }


    public void AddToTrain(GameObject unitToAdd)
    {
        unitsInTrain.Add(unitToAdd);

        unitTransforms.Add(new UnitTransform() { scale = unitsInTrain[unitsInTrain.Count - 1].transform.localScale, zPos = unitsInTrain[unitsInTrain.Count - 1].transform.position.z });
        unitsInTrain[unitsInTrain.Count - 1].transform.position = trainPoint[count].position;
        unitsInTrain[unitsInTrain.Count - 1].GetComponent<Move>().move = false;
        if (unitsInTrain[unitsInTrain.Count - 1].name == "good_dynamite_Trower(Clone)")
        {
            unitsInTrain[unitsInTrain.Count - 1].GetComponent<Dynamite_trower>().enabled = false;
        }
        else
        { 
            unitsInTrain[unitsInTrain.Count - 1].GetComponent<Attack>().enabled = false;
        }
        unitsInTrain[unitsInTrain.Count - 1].transform.localScale = new Vector3(.5f, .5f, .5f);
        unitsInTrain[unitsInTrain.Count - 1].transform.parent = trainPoint[count];
        count++;
    }

    public void ClearTrain()
    {
        unitsInTrain.Clear();
        unitTransforms.Clear();
        count = 0;
    }
}

public struct UnitTransform
{
    public Vector3 scale;
    public float zPos;
}
