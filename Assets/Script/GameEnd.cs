﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEnd : MonoBehaviour
{
    [SerializeField]
    CamMovement camMovement;
    [SerializeField]
    GameObject canvas, win, lose, continueButton;
    [SerializeField]
    AudioSource audioSource;
    [SerializeField]
    AudioClip winsound, losesound;

    private void Start()
    {
        continueButton.SetActive(false);
    }

    public void GameOver(Faction winner)
    {
        //destroy unnesesery objects
        Destroy(camMovement);
        Destroy(canvas);
        //start winning animation
        switch (winner)
        {
            case Faction.cowboiz:
                GameObject winAni = Instantiate(win);
                winAni.transform.parent = Camera.main.transform;
                winAni.transform.localPosition = new Vector3(0, 0, .8f);
                winAni.transform.localRotation = new Quaternion(0, 0, 0, 0);
                winAni.transform.localScale = new Vector3(.1f, .1f, .1f);
                audioSource.clip = winsound;
                break;
            case Faction.banditos:
                GameObject loseAni = Instantiate(lose);
                loseAni.transform.parent = Camera.main.transform;
                loseAni.transform.localPosition = new Vector3(0, 0, .8f);
                loseAni.transform.localRotation = new Quaternion(0, 0, 0, 0);
                loseAni.transform.localScale = new Vector3(.1f, .1f, .1f);
                audioSource.clip = losesound;
                break;
            default:
                break;
        }
        //play sound
        audioSource.loop = false;
        audioSource.Play();
        //set contiunue button active
        continueButton.SetActive(true);
    }
}
