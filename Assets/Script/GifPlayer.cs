﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GifPlayer : MonoBehaviour
{
    [SerializeField]
    Sprite[] frames;
    [SerializeField]
    int fps;
    [SerializeField]
    bool loop;

    SpriteRenderer spriteRenderer;
    float index;
    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        index += Time.deltaTime * fps;
        spriteRenderer.sprite = frames[Mathf.FloorToInt(index)];
        if (index >= frames.Length - 1)
        {
            if (loop == true)
            {
                index = 0;
            }
            else
            {
                Destroy(this);
            }
        }
    }
}
